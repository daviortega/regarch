import {
  IInstructionParsedBasic,
  IInstructionParsedComplex,
  IParsedPattern,
  IProteinArch,
  IRegArch,
} from './interfaces';
import { parseArch, parseRegArch } from './Interpreters';
import { getResourceFromArchFeature, isInstructionParsedComplex, isRegArchInstruction } from './RegArchUtils';

interface IMatchPos {
  match: boolean;
  lastParsedArchIndex: number;
  lastInstructionIndex: number;
}

interface IMatches {
  isMatch: boolean;
  matches: Array<{ matchPosition: number; matchingInstructionIndex: number }>;
  interval: [number, number];
}

class RegArch {
  private patterns: IParsedPattern[];
  constructor(regArch: IRegArch) {
    this.patterns = parseRegArch(regArch);
  }

  public exec(archs: IProteinArch[]) {
    return archs.map((arch, i) => {
      const parsedArch = parseArch(arch);
      return this.match(parsedArch);
    });
  }

  private match(parsedArch: string[]) {
    for (const pattern of this.patterns) {
      if (this.matchPos(parsedArch, 0, pattern, 0, false).match && this.matchNpos(parsedArch, pattern)) {
        return true;
      }
    }
    return false;
  }

  private matchNpos(parsedArch: string[], pattern: IParsedPattern): boolean {
    for (const instruction of pattern.npos) {
      if (isInstructionParsedComplex(instruction)) {
        return this.doSomething();
      } else {
        const interval = instruction[1];
        let counter = 0;
        for (const feature of parsedArch) {
          const regEx = new RegExp(instruction[0]);
          if (feature.match(regEx)) {
            counter++;
          }
          if (counter > interval[1]) {
            return false;
          }
        }
        if (counter < interval[0]) {
          return false;
        }
      }
    }
    return true;
  }

  private matchPos(
    parsedArch: string[],
    parsedArchIndexStart: number,
    pattern: IParsedPattern,
    patternIndex: number,
    mustMatchNext: boolean,
  ): IMatchPos {
    let noMoreArch = false;
    let parsedArchIndex = parsedArchIndexStart;
    let instructionIndex = patternIndex;
    for (instructionIndex; instructionIndex < pattern.pos.length; instructionIndex++) {
      const instructions = pattern.pos[instructionIndex];
      if (noMoreArch) {
        return {
          lastInstructionIndex: instructionIndex,
          lastParsedArchIndex: parsedArchIndex,
          match: false,
        };
      }
      let listOfInstructions: IInstructionParsedBasic[] = [];
      if (isInstructionParsedComplex(instructions)) {
        listOfInstructions = this.flatInstructions(instructions);
      } else {
        listOfInstructions = [instructions];
      }
      let skip = false;
      listOfInstructions.forEach(instruction => {
        // console.log(JSON.stringify(instruction))
        if (isRegArchInstruction(instruction) && instruction[0].match(/\^/) && instructionIndex === 0) {
          mustMatchNext = true;
          skip = true;
        }
        if (isRegArchInstruction(instruction) && instruction[0].match(/\$/)) {
          noMoreArch = true;
          if (parsedArchIndex < parsedArch.length) {
            while (parsedArchIndex < parsedArch.length) {
              const feature = parsedArch[parsedArchIndex];
              if (pattern.resources.has(getResourceFromArchFeature(feature))) {
                return {
                  lastInstructionIndex: instructionIndex,
                  lastParsedArchIndex: parsedArchIndex,
                  match: false,
                };
              } else {
                parsedArchIndex++;
              }
            }
            return {
              lastInstructionIndex: instructionIndex,
              lastParsedArchIndex: parsedArchIndex,
              match: true,
            };
          }
          skip = true;
        }
      });
      if (skip) {
        continue;
      }
      const tempParsedArchIndex = parsedArchIndex;
      const matches = listOfInstructions.map(
        (instruction): IMatches => {
          parsedArchIndex = tempParsedArchIndex;
          const counter: Array<{ matchPosition: number; matchingInstructionIndex: number }> = [];
          let skips = 0;
          const interval = instruction[1];
          while (parsedArchIndex < parsedArch.length) {
            const feature = parsedArch[parsedArchIndex];
            if (pattern.resources.has(getResourceFromArchFeature(feature))) {
              const regEx = new RegExp(instruction[0]);
              if (feature.match(regEx)) {
                counter.push({
                  matchPosition: parsedArchIndex,
                  matchingInstructionIndex: instructionIndex,
                });
                parsedArchIndex++;
                mustMatchNext = true;
              } else if (!mustMatchNext) {
                parsedArchIndex++;
                skips++;
                continue;
              } else if (interval[0] === 0) {
                parsedArchIndex++;
                continue;
              } else {
                break;
              }
            } else {
              parsedArchIndex++;
              skips++;
            }
          }
          if (counter.length < interval[0]) {
            return {
              interval,
              isMatch: false,
              matches: counter,
            };
          }
          if (interval[1] === Infinity) {
            const possibleValidCounts = counter.slice(interval[0] - 1);
            const possibleResults = possibleValidCounts.map(
              (index): IMatchPos => {
                return this.matchPos(parsedArch, index.matchPosition + 1, pattern, instructionIndex + 1, true);
              },
            );
            const allValidForwardMatches: Array<{ matchPosition: number; matchingInstructionIndex: number }> = [];
            possibleResults.forEach(result => {
              if (result.match) {
                allValidForwardMatches.push({
                  matchPosition: result.lastParsedArchIndex,
                  matchingInstructionIndex: result.lastInstructionIndex,
                });
              }
            });
            const mostForwardMatch = allValidForwardMatches.sort((a, b) => {
              if (a.matchingInstructionIndex === b.matchingInstructionIndex) {
                return b.matchPosition - a.matchPosition;
              }
              return b.matchingInstructionIndex - a.matchingInstructionIndex;
            });
            if (!mostForwardMatch.length) {
              return {
                interval,
                isMatch: false,
                matches: counter,
              };
            }
            counter.push(mostForwardMatch[0]);
            return {
              interval,
              isMatch: true,
              matches: counter,
            };
          }
          if (interval[1] === 0 && counter.length) {
            return {
              interval,
              isMatch: false,
              matches: counter,
            };
          }
          if (counter.length > interval[1]) {
            if (instructionIndex === pattern.pos.length - 1) {
              return {
                interval,
                isMatch: true,
                matches: counter,
              };
            }
            if (instruction[instructionIndex + 1] === '$@ra') {
              return {
                interval,
                isMatch: false,
                matches: counter,
              };
            }
            const newStart = counter[interval[1] - 1].matchPosition + 1;
            const matchForward = this.matchPos(parsedArch, newStart, pattern, instructionIndex + 1, true);
            counter.push({
              matchPosition: matchForward.lastParsedArchIndex,
              matchingInstructionIndex: matchForward.lastInstructionIndex,
            });
            return {
              interval,
              isMatch: matchForward.match,
              matches: counter,
            };
          }
          return {
            interval,
            isMatch: true,
            matches: counter,
          };
        },
      );
      const allMatches = matches.map(m => m.isMatch);
      if (allMatches.indexOf(false) !== -1) {
        const maybeNotFalse = matches
          .filter(m => !m.isMatch && m.interval[0] === 0 && m.interval[1] === 0)
          .map(m => m.matches);
        const allFalsyMatches: Array<{ matchPosition: number; matchingInstructionIndex: number }> = [];
        for (const falsy of maybeNotFalse) {
          falsy.forEach(f => allFalsyMatches.push(f));
        }
        const altMatchPositionLimit = allFalsyMatches.reduce(
          (acc: number, val: { matchPosition: number; matchingInstructionIndex: number }): number => {
            if (val.matchPosition < acc) {
              return val.matchPosition;
            }
            return acc;
          },
          Infinity,
        );

        const alternativeWaysForward = matches.filter(match => {
          if (match.isMatch) {
            const matchesBeforeLimit = match.matches.filter(m => {
              return m.matchPosition < altMatchPositionLimit && m.matchingInstructionIndex === instructionIndex;
            });
            return matchesBeforeLimit.length;
          }
          return false;
        });
        alternativeWaysForward.forEach(match => {
          match.matches = match.matches.filter(m => {
            return m.matchPosition < altMatchPositionLimit;
          });
        });
        if (alternativeWaysForward.length) {
          const allAlternativeWaysForward = alternativeWaysForward.map(m => m.matches);
          const latestCommonMatch = this.findLatestCommonMatch(allAlternativeWaysForward);
          if (latestCommonMatch.matchPosition === -1) {
            return {
              lastInstructionIndex: latestCommonMatch.matchingInstructionIndex,
              lastParsedArchIndex: latestCommonMatch.matchPosition,
              match: false,
            };
          } else {
            parsedArchIndex = latestCommonMatch.matchPosition + 1;
            instructionIndex = latestCommonMatch.matchingInstructionIndex;
            const nextRule = this.matchPos(parsedArch, parsedArchIndex, pattern, instructionIndex + 1, true);
            if (nextRule.match) {
              return {
                lastInstructionIndex: instructionIndex - 1,
                lastParsedArchIndex: parsedArchIndex - 1,
                match: true,
              };
            } else {
              return {
                lastInstructionIndex: instructionIndex,
                lastParsedArchIndex: altMatchPositionLimit,
                match: false,
              };
            }
          }
        } else {
          return {
            lastInstructionIndex: instructionIndex,
            lastParsedArchIndex: altMatchPositionLimit,
            match: false,
          };
        }
      } else {
        const allMatchesPositionInfo = matches.map(m => m.matches);
        const latestCommonMatch = this.findLatestCommonMatch(allMatchesPositionInfo);
        if (latestCommonMatch.matchPosition === -1) {
          return {
            lastInstructionIndex: latestCommonMatch.matchingInstructionIndex,
            lastParsedArchIndex: latestCommonMatch.matchPosition,
            match: false,
          };
        }
        parsedArchIndex = latestCommonMatch.matchPosition + 1;
        instructionIndex = latestCommonMatch.matchingInstructionIndex;
      }
    }
    return {
      lastInstructionIndex: instructionIndex - 1,
      lastParsedArchIndex: parsedArchIndex - 1,
      match: true,
    };
  }

  private findLatestCommonMatch(
    matches: Array<Array<{ matchPosition: number; matchingInstructionIndex: number }>>,
  ): { matchPosition: number; matchingInstructionIndex: number } {
    let validSet = matches[0];
    const noCommonMatch = {
      matchPosition: -1,
      matchingInstructionIndex: -1,
    };
    for (let i = 1; i < matches.length; i++) {
      const newValidSet = [];
      if (matches[i].length) {
        for (let j = 1; j < validSet.length; j++) {
          for (const match of matches[i]) {
            if (
              match.matchPosition === validSet[j].matchPosition &&
              match.matchingInstructionIndex === validSet[j].matchingInstructionIndex
            ) {
              newValidSet.push(match);
            }
          }
        }
        if (!newValidSet.length) {
          return noCommonMatch;
        }
        validSet = newValidSet;
      }
    }
    validSet = validSet.sort((a, b) => {
      if (a.matchingInstructionIndex === b.matchingInstructionIndex) {
        return b.matchPosition - a.matchPosition;
      }
      return b.matchingInstructionIndex - a.matchingInstructionIndex;
    });
    return validSet[0];
  }

  private flatInstructions(instructions: IInstructionParsedComplex): IInstructionParsedBasic[] {
    return instructions.reduce(
      (acc: IInstructionParsedBasic[], instruction: IInstructionParsedComplex | IInstructionParsedBasic) => {
        return isInstructionParsedComplex(instruction)
          ? acc.concat(this.flatInstructions(instruction))
          : acc.concat([instruction]);
      },
      [],
    );
  }

  private doSomething() {
    return true;
  }
}

export { RegArch };
