interface IProteinArch {
  pa: {
    [feature: string]: Array<Array<string | number>> | undefined;
  };
  [details: string]: any;
}

interface IInstructionBasic {
  name: string;
  resource: string;
  count?: string;
}

interface IInstructionComplex extends Array<IInstructionBasic | IInstructionComplex> {}

interface IPattern {
  pos: Array<IInstructionBasic | IInstructionComplex>;
  npos: IInstructionBasic[];
  [details: string]: any;
}

type IInstructionParsedBasic = [string, [number, number]];

interface IInstructionParsedComplex extends Array<IInstructionParsedBasic | IInstructionParsedComplex> {}

interface IParsedPattern {
  npos: IInstructionParsedComplex;
  pos: IInstructionParsedComplex;
  resources: Set<string>;
}

interface IRegArch {
  patterns: IPattern[];
  details?: any;
}

export {
  IProteinArch,
  IInstructionBasic,
  IInstructionComplex,
  IInstructionParsedBasic,
  IInstructionParsedComplex,
  IParsedPattern,
  IPattern,
  IRegArch,
};
