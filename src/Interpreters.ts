import { IParsedPattern, IProteinArch, IRegArch } from './interfaces';
import { RegArchParser } from './RegArchParser';

interface IFeature {
  resource: string;
  name: string;
  pos: [number, number];
}

const parseArch = (pa: IProteinArch): string[] => {
  const resources = Object.keys(pa.pa);
  const arch: IFeature[] = [];
  resources.forEach(resource => {
    const instruction = pa.pa[resource];
    if (typeof instruction !== 'undefined') {
      instruction.forEach(feature => {
        const name = feature[0];
        const start = feature[1];
        const end = feature[2];
        if (typeof name === 'string' && typeof start === 'number' && typeof end === 'number') {
          arch.push({
            name,
            pos: [start, end],
            resource,
          });
        }
      });
    }
  });
  arch.sort((a, b) => {
    return a.pos[0] - b.pos[0];
  });
  const expressions: string[] = [];
  arch.forEach(feature => {
    expressions.push(`${feature.pos[0]}|${feature.name}@${feature.resource}|${feature.pos[1]}`);
  });
  return expressions;
};

const parseRegArch = (regArch: IRegArch): IParsedPattern[] => {
  const parsedRegArch: IParsedPattern[] = [];
  for (const patterns of regArch.patterns) {
    const parser = new RegArchParser();
    parsedRegArch.push(parser.build(patterns));
  }
  return parsedRegArch;
};

export { parseArch, parseRegArch };
