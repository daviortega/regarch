import {
  IInstructionBasic,
  IInstructionComplex,
  IInstructionParsedBasic,
  IInstructionParsedComplex,
} from './interfaces';

const validateArch = (arch: string[]): string[] => {
  for (const feature of arch) {
    if (!isValidArchFeature(feature)) {
      throw Error(`${feature} is not a valid feature.`);
    }
  }
  return arch;
};

const isValidArchFeature = (archFeature: string) => {
  return archFeature.match(/^[0-9]{1,}\|[^@]+@[^@]+\|[0-9]{1,}$/);
};

const isInstruction = (instruction: IInstructionBasic | IInstructionComplex): instruction is IInstructionBasic => {
  return (instruction as IInstructionBasic).name !== undefined;
};

const isInstructionParsedComplex = (
  instruction: IInstructionParsedBasic | IInstructionParsedComplex,
): instruction is IInstructionParsedComplex => {
  const instructionParsedComplex = instruction as IInstructionParsedComplex;
  return typeof instructionParsedComplex[0] !== 'string'; // instructionParsedBasic.length === 2  && instructionParsedBasic[1].length === 2
};

const isRegArchInstruction = (instruction: IInstructionParsedBasic) => {
  return instruction[0].match(/@ra$/);
};

const getResourceFromArchFeature = (archFeature: string) => {
  return archFeature.split('|')[1].split('@')[1];
};

export {
  getResourceFromArchFeature,
  isInstruction,
  isInstructionParsedComplex,
  isRegArchInstruction,
  isValidArchFeature,
  validateArch,
};
