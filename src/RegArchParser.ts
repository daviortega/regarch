import {
  IInstructionBasic,
  IInstructionComplex,
  IInstructionParsedBasic,
  IInstructionParsedComplex,
  IParsedPattern,
  IPattern,
} from './interfaces';
import { isInstruction } from './RegArchUtils';

class RegArchParser {
  private resources: Set<string>;

  constructor() {
    this.resources = new Set();
  }

  public build(pattern: IPattern): IParsedPattern {
    const parsed = {
      npos: this.parsePattern(pattern.npos),
      pos: this.parsePattern(pattern.pos),
      resources: this.resources,
    };
    return parsed;
  }

  private parsePattern(instructions: Array<IInstructionBasic | IInstructionComplex>) {
    const parsedPattern: IInstructionParsedComplex = [];
    if (instructions) {
      instructions.forEach(instruction => {
        if (!isInstruction(instruction)) {
          parsedPattern.push(this.parsePattern(instruction));
        } else {
          const expr = instruction.name + '@' + instruction.resource;
          this.resources.add(instruction.resource);
          const parsedInstruction: IInstructionParsedBasic = [expr, [1, 1]];
          if (instruction.count) {
            parsedInstruction[1] = this.parseCount(instruction.count);
          }
          parsedPattern.push(parsedInstruction);
        }
      });
    }
    return parsedPattern;
  }

  private parseCount(countString: string): [number, number] {
    const toBeParsed = countString
      .replace('{', '')
      .replace('}', '')
      .split(',');
    const interval: [number, number] = [1, Infinity];
    if (toBeParsed.length > interval.length) {
      throw new Error('Invalid count value (too many commas): ' + countString);
    }
    toBeParsed.forEach((n, i) => {
      if (n.match(/^\d+$/)) {
        interval[i] = parseInt(n, 10);
        if (toBeParsed.length === 1) {
          interval[i + 1] = parseInt(n, 10);
        }
      } else if (n === '' && i !== 0) {
        interval[i] = Infinity;
      } else if (n === '' && i === 0) {
        throw Error('Invalid count value (must have at least one integer ): ' + countString);
      } else {
        throw new Error('Invalid count value (only integers no space): ' + countString);
      }
    });
    return interval;
  }
}

export { RegArchParser };
