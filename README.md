# RegArch - Regular Architecture

![npm](https://img.shields.io/npm/v/regarch.svg)
![License: CC0-1.0](https://img.shields.io/badge/License-CC0%201.0-blue.svg)

[![pipeline status](https://gitlab.com/daviortega/regarch/badges/master/pipeline.svg)](https://gitlab.com/daviortega/regarch/commits/master)
[![coverage report](https://gitlab.com/daviortega/regarch/badges/master/coverage.svg)](https://daviortega.gitlab.io/regarch/)

Javascript implementation of Regular Architectures (RegArch).

## What is RegArch?

We borrow the name from Regular Expressions to name our package Regular Architectures as a pattern matching tool tailored to find proteins using architecture patterns.

## Install

``` bash
$ npm install regarch
```

## Example usage

``` typescript
import { RegArch } from "regarch";

const ra = new RegArch(pattern)
const results = ra.exec(listOfAnnotatedProteins)
```

## Learn more

The details on how to build the patterns and how to format protein annotations can be found in the [manual](https://gitlab.com/daviortega/regarch/blob/master/RegArchManual.md)

If there are any issues, bugs or feature request, please open a new issue [here](https://gitlab.com/daviortega/regarch/issues).

Written with ❤ in Typescript.