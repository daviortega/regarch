import { expect } from "chai";

import { IRegArch } from "../src/interfaces";
import { RegArch } from "../src/RegArch";

import sampleData from '../testdata/annotation-sample-input.json'

const archs = [
    "CheW | CheW",
    "CheW | Response_reg",
    "CheW",
    "Hpt | P2 | H-kinase_dim | HATPase_c | CheW",
    "Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW",
    "Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg",
    "CheW | CheW | CheW",
    "Response_reg | NMT1_2 | CheW",
    "CheW | CheR",
    "Response_reg | CheW",
    "TM | Cache_1 | TM | HAMP | MCPsignal",
    "HAMP | MCPsignal",
    "PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal",
    "**",
    "**",
    "TM | TM | MCPsignal",
    "TM | MCPsignal | Rhodanese",
    "TM | TM | TM | TM | MCPsignal",
    "TM | TM | TM | TM | TM | TM | MCPsignal",
    "TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal",
    "TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal",
    "TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal",
    "TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal",
    "TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal",
]


describe('RegArch', () => {
    describe('exec', () => {
        describe('npos patterns', () => {
            describe('Single Rule - If broken, fix this first', () => {
                it('Match protein architectures with only 1 match, anywhere in the sequence, to a single CheW domain from pfam28', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [
                                    {
                                        name: 'CheW',
                                        resource: 'pfam28'
                                    }
                                ],
                                pos: []
                            }
                        ]
                    }
                    const expected = [
                        false, // CheW | CheW
                        true,  // CheW | Response_reg
                        true,  // CheW
                        true,  // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        false,  // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        true,  // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        false,  // CheW | CheW | CheW
                        true,  // Response_reg | NMT1_2 | CheW
                        true,  // CheW | CheR
                        true,  // Response_reg | CheW
                        false, // TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // HAMP | MCPsignal
                        false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // **
                        false, // **
                        false, // TM | TM | MCPsignal
                        false, // TM | MCPsignal | Rhodanese
                        false, // TM | TM | TM | TM | MCPsignal
                        false, // TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false, // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Match protein architectures with 1 or 2 matches, anywhere in the sequence, to a single CheW domain from pfam28', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [
                                    {
                                        count: '{1,2}',
                                        name: 'CheW',
                                        resource: 'pfam28'
                                    }
                                ],
                                pos: []
                            }
                        ]
                    }
                    const expected = [
                        true,  // CheW | CheW
                        true,  // CheW | Response_reg
                        true,  // CheW
                        true,  // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        true,  // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        true,  // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        false, // CheW | CheW | CheW
                        true,  // Response_reg | NMT1_2 | CheW
                        true,  // CheW | CheR
                        true,  // Response_reg | CheW
                        false, // TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // HAMP | MCPsignal
                        false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // **
                        false, // **
                        false, // TM | TM | MCPsignal
                        false, // TM | MCPsignal | Rhodanese
                        false, // TM | TM | TM | TM | MCPsignal
                        false, // TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false, // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Match protein architectures with 2 or 3 matches, anywhere in the sequence, to a single CheW domain from pfam28', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [
                                    {
                                        count: '{2,3}',
                                        name: 'CheW',
                                        resource: 'pfam28'
                                    }
                                ],
                                pos: []
                            }
                        ]
                    }
                    const expected = [
                        true,  // [0]  CheW | CheW
                        false, // [1]  CheW | Response_reg
                        false, // [2]  CheW
                        false, // [3]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        true,  // [4]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        false, // [5]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        true,  // [6]  CheW | CheW | CheW
                        false, // [7]  Response_reg | NMT1_2 | CheW
                        false, // [8]  CheW | CheR
                        false, // [9]  Response_reg | CheW
                        false, // [10] TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // [11] HAMP | MCPsignal
                        false, // [12] PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // [13] **
                        false, // [14] **
                        false, // [15] TM | TM | MCPsignal
                        false, // [16] TM | MCPsignal | Rhodanese
                        false, // [17] TM | TM | TM | TM | MCPsignal
                        false, // [18] TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // [19] TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [20] TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [21] TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [22] TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [23] TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Match protein architectures with 3 or more transmembrane regions, anywhere in the sequence', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [
                                    {
                                        count: '{3,}',
                                        name: 'TM',
                                        resource: 'das'
                                    }
                                ],
                                pos: []
                            }
                        ]
                    }
                    const expected = [
                        false, // [0]  CheW | CheW
                        false, // [1]  CheW | Response_reg
                        false, // [2]  CheW
                        false, // [3]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        false, // [4]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        false, // [5]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        false, // [6]  CheW | CheW | CheW
                        false, // [7]  Response_reg | NMT1_2 | CheW
                        false, // [8]  CheW | CheR
                        false, // [9]  Response_reg | CheW
                        false, // [10] TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // [11] HAMP | MCPsignal
                        false, // [12] PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // [13] **
                        false, // [14] **
                        false, // [15] TM | TM | MCPsignal
                        false, // [16] TM | MCPsignal | Rhodanese
                        true,  // [17] TM | TM | TM | TM | MCPsignal
                        true,  // [18] TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // [19] TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [20] TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [21] TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [22] TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [23] TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Match protein architectures without transmembrane regions', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [
                                    {
                                        count: '{0}',
                                        name: 'TM',
                                        resource: 'das'
                                    }
                                ],
                                pos: []
                            }
                        ]
                    }
                    const expected = [
                        true,  // CheW | CheW
                        true,  // CheW | Response_reg
                        true,  // CheW
                        true,  // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        true,  // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        true,  // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        true,  // CheW | CheW | CheW
                        true,  // Response_reg | NMT1_2 | CheW
                        true,  // CheW | CheR
                        true,  // Response_reg | CheW
                        false, // TM | Cache_1 | TM | HAMP | MCPsignal
                        true,  // HAMP | MCPsignal
                        false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        true,  // **
                        false, // **
                        false, // TM | TM | MCPsignal
                        false, // TM | MCPsignal | Rhodanese
                        false, // TM | TM | TM | TM | MCPsignal
                        false, // TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false, // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Filter proteins with 1 domain from pfam28', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [
                                    {
                                        count: '{1}',
                                        name: '.*',
                                        resource: 'pfam28'
                                    }
                                ],
                                pos: []
                            }
                        ]
                    }
                    const expected = [
                        false, // [0]  CheW | CheW
                        false, // [1]  CheW | Response_reg
                        true,  // [2]  CheW
                        false, // [3]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        false, // [4]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        false, // [5]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        false, // [6]  CheW | CheW | CheW
                        false, // [7]  Response_reg | NMT1_2 | CheW
                        false, // [8]  CheW | CheR
                        false, // [9]  Response_reg | CheW
                        false, // [10] TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // [11] HAMP | MCPsignal
                        false, // [12] PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // [13] **
                        false, // [14] **
                        true,  // [15] TM | TM | MCPsignal
                        false, // [16] TM | MCPsignal | Rhodanese
                        true,  // [17] TM | TM | TM | TM | MCPsignal
                        true,  // [18] TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // [19] TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [20] TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [21] TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [22] TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [23] TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
            })
            describe('Multiple Rules - AND mode', () => {
                it('Match protein architectures with at least 1 match to CheW domain in pfam28 AND only 1 match to HATPase_c domain in pfam28', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [
                                    {
                                        name: 'CheW',
                                        resource: 'pfam28'
                                    },
                                    {
                                        count: '{1}',
                                        name: 'HATPase_c',
                                        resource: 'pfam28'
                                    }
                                ],
                                pos: []
                            }
                        ]
                    }
                    const expected = [
                        false, // [0]  CheW | CheW
                        false, // [1]  CheW | Response_reg
                        false, // [2]  CheW
                        true,  // [3]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        false,  // [4]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        true,  // [5]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        false, // [6]  CheW | CheW | CheW
                        false, // [7]  Response_reg | NMT1_2 | CheW
                        false, // [8]  CheW | CheR
                        false, // [9]  Response_reg | CheW
                        false, // [10] TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // [11] HAMP | MCPsignal
                        false, // [12] PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // [13] **
                        false, // [14] **
                        false, // [15] TM | TM | MCPsignal
                        false, // [16] TM | MCPsignal | Rhodanese
                        false, // [17] TM | TM | TM | TM | MCPsignal
                        false, // [18] TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // [19] TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [20] TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [21] TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [22] TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [23] TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Match protein architectures with at least 1 match to CheW domain in pfam28 AND only 1 match to HATPase_c domain in pfam28 AND with no TM regions', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [
                                    {
                                        count: '{1,}',
                                        name: 'CheW',
                                        resource: 'pfam28'
                                    },
                                    {
                                        count: '{1}',
                                        name: 'HATPase_c',
                                        resource: 'pfam28'
                                    },
                                    {
                                        count: '{0}',
                                        name: 'Response_reg',
                                        resource: 'pfam28'
                                    }
                                ],
                                pos: []
                            }
                        ]
                    }
                    const expected = [
                        false, // [0]  CheW | CheW
                        false, // [1]  CheW | Response_reg
                        false, // [2]  CheW
                        true,  // [3]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        true,  // [4]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        false, // [5]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        false, // [6]  CheW | CheW | CheW
                        false, // [7]  Response_reg | NMT1_2 | CheW
                        false, // [8]  CheW | CheR
                        false, // [9]  Response_reg | CheW
                        false, // [10] TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // [11] HAMP | MCPsignal
                        false, // [12] PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // [13] **
                        false, // [14] **
                        false, // [15] TM | TM | MCPsignal
                        false, // [16] TM | MCPsignal | Rhodanese
                        false, // [17] TM | TM | TM | TM | MCPsignal
                        false, // [18] TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // [19] TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [20] TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [21] TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [22] TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [23] TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
            })
             describe('Multiple Rules - OR mode', () => {
                it('Match protein architectures with at least 2 matches to CheW domain in pfam28 OR only 1 match to HATPase_c domain in pfam28', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [
                                    {
                                        count: '{2,}',
                                        name: 'CheW',
                                        resource: 'pfam28'
                                    }
                                ],
                                pos: []
                            },
                            {
                                npos: [
                                    {
                                        count: '{1}',
                                        name: 'HATPase_c',
                                        resource: 'pfam28'
                                    }
                                ],
                                pos: []
                            }
                        ]
                    }
                    const expected = [
                        true,  // [0]  CheW | CheW
                        false, // [1]  CheW | Response_reg
                        false, // [2]  CheW
                        true,  // [3]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        true,  // [4]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        true,  // [5]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        true,  // [6]  CheW | CheW | CheW
                        false, // [7]  Response_reg | NMT1_2 | CheW
                        false, // [8]  CheW | CheR
                        false, // [9]  Response_reg | CheW
                        false, // [10] TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // [11] HAMP | MCPsignal
                        false, // [12] PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // [13] **
                        false, // [14] **
                        false, // [15] TM | TM | MCPsignal
                        false, // [16] TM | MCPsignal | Rhodanese
                        false, // [17] TM | TM | TM | TM | MCPsignal
                        false, // [18] TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // [19] TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [20] TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [21] TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [22] TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [23] TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Match protein architectures with at least 2 match to CheW domain in pfam28 OR only 1 match to HATPase_c domain in pfam28 OR only 1 matches to Response_reg in pfam28', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [
                                    {
                                        count: '{2,}',
                                        name: 'CheW',
                                        resource: 'pfam28'
                                    }
                                ],
                                pos: []
                            },
                            {
                                npos: [
                                    {
                                        count: '{1}',
                                        name: 'HATPase_c',
                                        resource: 'pfam28'
                                    }
                                ],
                                pos: []
                            },
                            {
                                npos: [
                                    {
                                        count: '{1}',
                                        name: 'Response_reg',
                                        resource: 'pfam28'
                                    }
                                ],
                                pos: []
                            }
                        ]
                    }
                    const expected = [
                        true,  // [0]  CheW | CheW
                        true,  // [1]  CheW | Response_reg
                        false, // [2]  CheW
                        true,  // [3]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        true,  // [4]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        true,  // [5]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        true,  // [6]  CheW | CheW | CheW
                        true,  // [7]  Response_reg | NMT1_2 | CheW
                        false, // [8]  CheW | CheR
                        true,  // [9]  Response_reg | CheW
                        false, // [10] TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // [11] HAMP | MCPsignal
                        false, // [12] PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // [13] **
                        false, // [14] **
                        false, // [15] TM | TM | MCPsignal
                        false, // [16] TM | MCPsignal | Rhodanese
                        false, // [17] TM | TM | TM | TM | MCPsignal
                        false, // [18] TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // [19] TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [20] TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [21] TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [22] TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [23] TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
            })
        })
        describe('pos patterns', () => {
            describe('Simple pos rules', () => {
                it('Match protein architectures starting with 1 match to a CheW domain from Pfam28', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [],
                                pos: [
                                    {
                                        name: '^',
                                        resource: 'ra'
                                    },
                                    {
                                        name: 'CheW',
                                        resource: 'pfam28'
                                    }
                                ]
                            }
                        ]
                    }
                    const expected = [
                        true,  // CheW | CheW
                        true,  // CheW | Response_reg
                        true,  // CheW
                        false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        true,  // CheW | CheW | CheW
                        false, // Response_reg | NMT1_2 | CheW
                        true,  // CheW | CheR
                        false, // Response_reg | CheW
                        false, // TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // HAMP | MCPsignal
                        false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // **
                        false, // **
                        false, // TM | TM | MCPsignal
                        false, // TM | MCPsignal | Rhodanese
                        false, // TM | TM | TM | TM | MCPsignal
                        false, // TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false  // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Match protein architectures starting with 2 CheW domains from Pfam28', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [],
                                pos: [
                                    {
                                        name: '^',
                                        resource: 'ra'
                                    },
                                    {
                                        count: '{2}',
                                        name: 'CheW',
                                        resource: 'pfam28',
                                    },
                                ]
                            }
                        ]
                    }
                    const expected = [
                        true,  // [0]  CheW | CheW
                        false, // [1]  CheW | Response_reg
                        false, // [2]  CheW
                        false, // [3]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        false, // [4]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        false, // [5]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        true, // [6]  CheW | CheW | CheW
                        false, // [7]  Response_reg | NMT1_2 | CheW
                        false, // [8]  CheW | CheR
                        false, // [9]  Response_reg | CheW
                        false, // [10] TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // [11] HAMP | MCPsignal
                        false, // [12] PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // [13] **
                        false, // [14] **
                        false, // [15] TM | TM | MCPsignal
                        false, // [16] TM | MCPsignal | Rhodanese
                        false, // [17] TM | TM | TM | TM | MCPsignal
                        false, // [18] TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // [19] TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [20] TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [21] TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [22] TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false  // [23] TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Match protein architectures starting with 2 or more CheW domains from Pfam28', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [],
                                pos: [
                                    {
                                        name: '^',
                                        resource: 'ra'
                                    },
                                    {
                                        count: '{2,}',
                                        name: 'CheW',
                                        resource: 'pfam28',
                                    },
                                ]
                            }
                        ]
                    }
                    const expected = [
                        true,  // [0]  CheW | CheW
                        false, // [1]  CheW | Response_reg
                        false, // [2]  CheW
                        false, // [3]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        false, // [4]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        false, // [5]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        true,  // [6]  CheW | CheW | CheW
                        false, // [7]  Response_reg | NMT1_2 | CheW
                        false, // [8]  CheW | CheR
                        false, // [9]  Response_reg | CheW
                        false, // [10] TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // [11] HAMP | MCPsignal
                        false, // [12] PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // [13] **
                        false, // [14] **
                        false, // [15] TM | TM | MCPsignal
                        false, // [16] TM | MCPsignal | Rhodanese
                        false, // [17] TM | TM | TM | TM | MCPsignal
                        false, // [18] TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // [19] TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [20] TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [21] TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [22] TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false  // [23] TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Match protein architectures starting with 1 TM followed by 1 Cache_2 domain from Pfam28', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [],
                                pos: [
                                    {
                                        name: '^',
                                        resource: 'ra'
                                    },
                                    {
                                        count: '{1}',
                                        name: 'TM',
                                        resource: 'das',
                                    },
                                    {
                                        count: '{1}',
                                        name: 'Cache_2',
                                        resource: 'pfam28',
                                    },
                                ]
                            }
                        ]
                    }
                    const expected = [
                        false, // [0] CheW | CheW
                        false, // [1] CheW | Response_reg
                        false, // [2] CheW
                        false, // [3] Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        false, // [4] Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        false, // [5] Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        false, // [6] CheW | CheW | CheW
                        false, // [7] Response_reg | NMT1_2 | CheW
                        false, // [8] CheW | CheR
                        false, // [9] Response_reg | CheW
                        false, // [10] TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // [11] HAMP | MCPsignal
                        false, // [12] PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // [13] **
                        false, // [14] **
                        false, // [15] TM | TM | MCPsignal
                        false, // [16] TM | MCPsignal | Rhodanese
                        false, // [17] TM | TM | TM | TM | MCPsignal
                        false, // [18] TM | TM | TM | TM | TM | TM | MCPsignal
                        true, // [19] TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        true,  // [20] TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [21] TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [22] TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        true  // [23] TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Match protein architectures starting with 1 TM follow by any 2 features and one or more TM', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [
                                    {
                                        count: '{0,}',
                                        name: '.*',
                                        resource: 'pfam28',

                                    }
                                ],
                                pos: [
                                    {
                                        name: '^',
                                        resource: 'ra'
                                    },
                                    {
                                        count: '{1}',
                                        name: 'TM',
                                        resource: 'das',
                                    },
                                    {
                                        count: '{2}',
                                        name: '.*',
                                        resource: '.*',
                                    },
                                    {
                                        count: '{1,}',
                                        name: 'TM',
                                        resource: 'das',
                                    },
                                ]
                            }
                        ]
                    }
                    const expected = [
                        false, // [0] CheW | CheW
                        false, // [1] CheW | Response_reg
                        false, // [2] CheW
                        false, // [3] Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        false, // [4] Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        false, // [5] Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        false, // [6] CheW | CheW | CheW
                        false, // [7] Response_reg | NMT1_2 | CheW
                        false, // [8] CheW | CheR
                        false, // [9] Response_reg | CheW
                        false, // [10] TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // [11] HAMP | MCPsignal
                        false, // [12] PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // [13] **
                        false, // [14] **
                        false, // [15] TM | TM | MCPsignal
                        false, // [16] TM | MCPsignal | Rhodanese
                        true,  // [17] TM | TM | TM | TM | MCPsignal
                        true,  // [18] TM | TM | TM | TM | TM | TM | MCPsignal
                        true,  // [19] TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        true,  // [20] TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        true,  // [21] TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        true,  // [22] TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false  // [23] TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Filter proteins sequences with any number of matches, anywhere in the sequence, to a single domain from pfam28', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [],
                                pos: [
                                    {
                                        name: 'CheW',
                                        resource: 'pfam28'
                                    }
                                ]
                            }
                        ]
                    }
                    const expected = [
                        true,  // CheW | CheW
                        true,  // CheW | Response_reg
                        true,  // CheW
                        true,  // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        true,  // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        true,  // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        true,  // CheW | CheW | CheW
                        true,  // Response_reg | NMT1_2 | CheW
                        true,  // CheW | CheR
                        true,  // Response_reg | CheW
                        false, // TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // HAMP | MCPsignal
                        false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // **
                        false, // **
                        false, // TM | TM | MCPsignal
                        false, // TM | MCPsignal | Rhodanese
                        false, // TM | TM | TM | TM | MCPsignal
                        false, // TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false  // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Non-positional and positional rules do not have the same output', () => {
                    const regArchPatternPos: IRegArch = {
                        patterns: [
                            {
                                npos: [],
                                pos: [
                                    {
                                        name: 'CheW',
                                        resource: 'pfam28'
                                    }
                                ]
                            }
                        ]
                    }
                    const regArchPatternNpos: IRegArch = {
                        patterns: [
                            {
                                npos: [
                                    {
                                        name: 'CheW',
                                        resource: 'pfam28'
                                    }
                                ],
                                pos: []
                            }
                        ]
                    }
                    const expected = [
                        true,  // CheW | CheW
                        true,  // CheW | Response_reg
                        true,  // CheW
                        true,  // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        true,  // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        true,  // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        true,  // CheW | CheW | CheW
                        true,  // Response_reg | NMT1_2 | CheW
                        true,  // CheW | CheR
                        true,  // Response_reg | CheW
                        false, // TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // HAMP | MCPsignal
                        false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // **
                        false, // **
                        false, // TM | TM | MCPsignal
                        false, // TM | MCPsignal | Rhodanese
                        false, // TM | TM | TM | TM | MCPsignal
                        false, // TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false  // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const raPos = new RegArch(regArchPatternPos)
                    const raNpos = new RegArch(regArchPatternNpos)
                    const resultsPos = raPos.exec(sampleData)
                    const resultsNpos = raNpos.exec(sampleData)
                    expect(resultsPos).not.eql(resultsNpos)
                })
                it('Two different rules should not give the same output', () => {
                    const regArchPatternPos: IRegArch = {
                        patterns: [
                            {
                                npos: [],
                                pos: [
                                    {
                                        name: 'CheW',
                                        resource: 'pfam28'
                                    }
                                ]
                            }
                        ]
                    }
                    const regArchPatternNpos: IRegArch = {
                        patterns: [
                            {
                                npos: [
                                    {
                                        name: 'MCPSignal',
                                        resource: 'pfam28'
                                    }
                                ],
                                pos: []
                            }
                        ]
                    }

                    const raPos = new RegArch(regArchPatternPos)
                    const raNpos = new RegArch(regArchPatternNpos)
                    const resultsPos = raPos.exec(sampleData)
                    const resultsNpos = raNpos.exec(sampleData)
                    expect(resultsPos).to.not.eql(resultsNpos)
                })
                it('Match protein architectures with 1 match to a CheW domain from Pfam28 and nothing else', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [],
                                pos: [
                                    {
                                        name: '^',
                                        resource: 'ra'
                                    },
                                    {
                                        name: 'CheW',
                                        resource: 'pfam28',
                                    },
                                    {
                                        name: '$',
                                        resource: 'ra',
                                    }
                                ]
                            }
                        ]
                    }
                    const expected = [
                        false, // [0]  CheW | CheW
                        false, // [1]  CheW | Response_reg
                        true,  // [2]  CheW
                        false, // [3]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        false, // [4]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        false, // [5]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        false, // [6]  CheW | CheW | CheW
                        false, // [7]  Response_reg | NMT1_2 | CheW
                        false, // [8]  CheW | CheR
                        false, // [9]  Response_reg | CheW
                        false, // [10] TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // [11] HAMP | MCPsignal
                        false, // [12] PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // [13] **
                        false, // [14] **
                        false, // [15] TM | TM | MCPsignal
                        false, // [16] TM | MCPsignal | Rhodanese
                        false, // [17] TM | TM | TM | TM | MCPsignal
                        false, // [18] TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // [19] TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [20] TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [21] TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [22] TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false  // [23] TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Match protein architectures that starts with TM-Cache_1 and end in MCPsignal', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [],
                                pos: [
                                    {
                                        name: '^',
                                        resource: 'ra'
                                    },
                                    {
                                        count: '{1}',
                                        name: 'TM',
                                        resource: 'das',
                                    },
                                    {
                                        count: '{1}',
                                        name: 'Cache_1',
                                        resource: 'pfam28',
                                    },
                                    {
                                        count: '{1,}',
                                        name: '.*',
                                        resource: '.*',
                                    },
                                    {
                                        count: '{1}',
                                        name: 'MCPsignal',
                                        resource: 'pfam28',
                                    },
                                    {
                                        name: '$',
                                        resource: 'ra',
                                    }
                                ]
                            }
                        ]
                    }
                    const expected = [
                        false, // [0]  CheW | CheW
                        false, // [1]  CheW | Response_reg
                        false, // [2]  CheW
                        false, // [3]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        false, // [4]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        false, // [5]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        false, // [6]  CheW | CheW | CheW
                        false, // [7]  Response_reg | NMT1_2 | CheW
                        false, // [8]  CheW | CheR
                        false, // [9]  Response_reg | CheW
                        true,  // [10] TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // [11] HAMP | MCPsignal
                        false, // [12] PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // [13] **
                        false, // [14] **
                        false, // [15] TM | TM | MCPsignal
                        false, // [16] TM | MCPsignal | Rhodanese
                        false, // [17] TM | TM | TM | TM | MCPsignal
                        false, // [18] TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // [19] TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [20] TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        true,  // [21] TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        true,  // [22] TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false  // [23] TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Filter sequences with starts with TM-Cache_2 and end in MCPsignal', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [],
                                pos: [
                                    {
                                        name: '^',
                                        resource: 'ra'
                                    },
                                    {
                                        count: '{1}',
                                        name: 'TM',
                                        resource: 'das',
                                    },
                                    {
                                        count: '{1}',
                                        name: 'Cache_2',
                                        resource: 'pfam28',
                                    },
                                    {
                                        count: '{1,}',
                                        name: '.*',
                                        resource: '.*',
                                    },
                                    {
                                        count: '{1}',
                                        name: 'MCPsignal',
                                        resource: 'pfam28',
                                    },
                                    {
                                        name: '$',
                                        resource: 'ra',
                                    }
                                ]
                            }
                        ]
                    }
                    const expected = [
                        false, // [0]  CheW | CheW
                        false, // [1]  CheW | Response_reg
                        false, // [2]  CheW
                        false, // [3]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        false, // [4]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        false, // [5]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        false, // [6]  CheW | CheW | CheW
                        false, // [7]  Response_reg | NMT1_2 | CheW
                        false, // [8]  CheW | CheR
                        false, // [9]  Response_reg | CheW
                        false, // [10] TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // [11] HAMP | MCPsignal
                        false, // [12] PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // [13] **
                        false, // [14] **
                        false, // [15] TM | TM | MCPsignal
                        false, // [16] TM | MCPsignal | Rhodanese
                        false, // [17] TM | TM | TM | TM | MCPsignal
                        false, // [18] TM | TM | TM | TM | TM | TM | MCPsignal
                        true,  // [19] TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        true,  // [20] TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        false, // [21] TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [22] TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        true   // [23] TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
                it('Filter sequences with starts with TM-Cache_1 followed by anything BUT another Cache_1 and end in MCPsignal', () => {
                    const regArchPattern: IRegArch = {
                        patterns: [
                            {
                                npos: [],
                                pos: [
                                    {
                                        name: '^',
                                        resource: 'ra'
                                    },
                                    {
                                        count: '{1}',
                                        name: 'TM',
                                        resource: 'das',
                                    },
                                    {
                                        count: '{1}',
                                        name: 'Cache_1',
                                        resource: 'pfam28',
                                    },
                                    [
                                        {
                                            count: '{1,}',
                                            name: '.*',
                                            resource: '.*',
                                        },
                                        {
                                            count: '{0}',
                                            name: 'Cache_1',
                                            resource: 'pfam28',
                                        },
                                    ],
                                    {
                                        count: '{1}',
                                        name: 'MCPsignal',
                                        resource: 'pfam28',
                                    },
                                    {
                                        name: '$',
                                        resource: 'ra',
                                    }
                                ]
                            }
                        ]
                    }
                    const expected = [
                        false, // [0]  CheW | CheW
                        false, // [1]  CheW | Response_reg
                        false, // [2]  CheW
                        false, // [3]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW
                        false, // [4]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
                        false, // [5]  Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
                        false, // [6]  CheW | CheW | CheW
                        false, // [7]  Response_reg | NMT1_2 | CheW
                        false, // [8]  CheW | CheR
                        false, // [9]  Response_reg | CheW
                        true,  // [10] TM | Cache_1 | TM | HAMP | MCPsignal
                        false, // [11] HAMP | MCPsignal
                        false, // [12] PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
                        false, // [13] **
                        false, // [14] **
                        false, // [15] TM | TM | MCPsignal
                        false, // [16] TM | MCPsignal | Rhodanese
                        false, // [17] TM | TM | TM | TM | MCPsignal
                        false, // [18] TM | TM | TM | TM | TM | TM | MCPsignal
                        false, // [19] TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [20] TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
                        true,  // [21] TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
                        false, // [22] TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
                        false  // [23] TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
                    ]
                    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
                })
            })
        })
		describe('Testing the behaviour of "count" for positional rules :: ', () => {
			it('Request protein sequences with 2 matches and nothing else to a single domain from pfam28', () => {
                const regArchPattern: IRegArch = {
                    patterns: [
                        {
                            npos: [],
                            pos: [
                                {
                                    name: '^',
                                    resource: 'ra'
                                },
                                {
                                    count: '{2}',
                                    name: 'CheW',
                                    resource: 'pfam28',
                                },
                                {
                                    name: '$',
                                    resource: 'ra',
                                }
                            ]
                        }
                    ]
                }
				const expected = [
					true, // CheW | CheW
					false, // CheW | Response_reg
					false, // CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
					false, // CheW | CheW | CheW
					false, // Response_reg | NMT1_2 | CheW
					false, // CheW | CheR
					false, // Response_reg | CheW
					false, // TM | Cache_1 | TM | HAMP | MCPsignal
					false, // HAMP | MCPsignal
					false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
					false, // **
					false, // **
					false, // TM | TM | MCPsignal
					false, // TM | MCPsignal | Rhodanese
					false, // TM | TM | TM | TM | MCPsignal
					false, // TM | TM | TM | TM | TM | TM | MCPsignal
					false,  // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
					false // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
				]
				    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
			})
			it('Request protein sequences with 3 matches and nothing else to a single domain from pfam28', () => {
				const regArchPattern: IRegArch = {
                    patterns: [
                        {
                            npos: [],
                            pos: [
                                {
                                    name: '^',
                                    resource: 'ra'
                                },
                                {
                                    count: '{3}',
                                    name: 'CheW',
                                    resource: 'pfam28',
                                },
                                {
                                    name: '$',
                                    resource: 'ra',
                                }
                            ]
                        }
                    ]
                }
				const expected = [
					false, // CheW | CheW
					false, // CheW | Response_reg
					false, // CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
					true, // CheW | CheW | CheW
					false, // Response_reg | NMT1_2 | CheW
					false, // CheW | CheR
					false, // Response_reg | CheW
					false, // TM | Cache_1 | TM | HAMP | MCPsignal
					false, // HAMP | MCPsignal
					false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
					false, // **
					false, // **
					false, // TM | TM | MCPsignal
					false, // TM | MCPsignal | Rhodanese
					false, // TM | TM | TM | TM | MCPsignal
					false, // TM | TM | TM | TM | TM | TM | MCPsignal
					false,  // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
					false // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
				]
				    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
			})
			it('Filter protein sequences with 2 or 3 matches and nothing else to a single domain from pfam28', () => {
				const regArchPattern: IRegArch = {
                    patterns: [
                        {
                            npos: [],
                            pos: [
                                {
                                    name: '^',
                                    resource: 'ra'
                                },
                                {
                                    count: '{2,3}',
                                    name: 'CheW',
                                    resource: 'pfam28',
                                },
                                {
                                    name: '$',
                                    resource: 'ra',
                                }
                            ]
                        }
                    ]
                }
				const expected = [
					true, // CheW | CheW
					false, // CheW | Response_reg
					false, // CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
					true, // CheW | CheW | CheW
					false, // Response_reg | NMT1_2 | CheW
					false, // CheW | CheR
					false, // Response_reg | CheW
					false, // TM | Cache_1 | TM | HAMP | MCPsignal
					false, // HAMP | MCPsignal
					false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
					false, // **
					false, // **
					false, // TM | TM | MCPsignal
					false, // TM | MCPsignal | Rhodanese
					false, // TM | TM | TM | TM | MCPsignal
					false, // TM | TM | TM | TM | TM | TM | MCPsignal
					false,  // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
					false // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
				]
				    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
			})
			it('Request protein sequences with 2 or 3 matches at the end of the sequence to a single domain from pfam28', () => {
                const regArchPattern: IRegArch = {
                    patterns: [
                        {
                            npos: [],
                            pos: [
                                {
                                    count: '{2,3}',
                                    name: 'CheW',
                                    resource: 'pfam28',
                                },
                                {
                                    name: '$',
                                    resource: 'ra',
                                }
                            ]
                        }
                    ]
                }
				const expected = [
					true, // CheW | CheW
					false, // CheW | Response_reg
					false, // CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
					true, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
					true, // CheW | CheW | CheW
					false, // Response_reg | NMT1_2 | CheW
					false, // CheW | CheR
					false, // Response_reg | CheW
					false, // TM | Cache_1 | TM | HAMP | MCPsignal
					false, // HAMP | MCPsignal
					false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
					false, // **
					false, // **
					false, // TM | TM | MCPsignal
					false, // TM | MCPsignal | Rhodanese
					false, // TM | TM | TM | TM | MCPsignal
					false, // TM | TM | TM | TM | TM | TM | MCPsignal
					false,  // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
					false // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
				]
				    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
			})
			it('Request protein sequences with 2 TM regions anywhere in the sequence with MCPsignal at the end', () => {
                const regArchPattern: IRegArch = {
                    patterns: [
                        {
                            npos: [
                                {
                                    count: '{2}',
                                    name: 'TM',
                                    resource: 'das',

                                }
                            ],
                            pos: [
                                {
                                    count: '{1}',
                                    name: 'MCPsignal',
                                    resource: 'pfam28',
                                },
                                {
                                    name: '$',
                                    resource: 'ra',
                                },
                            ]
                        }
                    ]
                }
				const expected = [
					false, // CheW | CheW
					false, // CheW | Response_reg
					false, // CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
					false, // CheW | CheW | CheW
					false, // Response_reg | NMT1_2 | CheW
					false, // CheW | CheR
					false, // Response_reg | CheW
					true, // TM | Cache_1 | TM | HAMP | MCPsignal
					false, // HAMP | MCPsignal
					true, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
					false, // **
					true, // **
					true,  // TM | TM | MCPsignal
					false,  // TM | TM | MCPsignal | Rhodanese
					false, // TM | TM | TM | TM | MCPsignal
					false, // TM | TM | TM | TM | TM | TM | MCPsignal
					true,  // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
					true, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
					true, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
					true, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
					true // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
				]
				    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
			})
		})
		describe('Testing the behaviour of wildcard for positional rules', () => {
			it('Filter protein sequences with any 2 pfam28 domains', () => {
				const regArchPattern: IRegArch = {
                    patterns: [
                        {
                            npos: [],
                            pos: [
                                {
                                    name: '^',
                                    resource: 'ra'
                                },
                                {
                                    count: '{2}',
                                    name: '.*',
                                    resource: 'pfam28',
                                },
                                {
                                    name: '$',
                                    resource: 'ra',
                                }
                            ]
                        }
                    ]
                }
				const expected = [
					true, // CheW | CheW
					true, // CheW | Response_reg
					false, // CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
					false, // CheW | CheW | CheW
					false, // Response_reg | NMT1_2 | CheW
					true, // CheW | CheR
					true, // Response_reg | CheW
					false, // TM | Cache_1 | TM | HAMP | MCPsignal
					true, // HAMP | MCPsignal
					false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
					false, // **
					false, // **
					false,  // TM | TM | MCPsignal
					true,  // TM | TM | MCPsignal | Rhodanese
					false, // TM | TM | TM | TM | MCPsignal
					false, // TM | TM | TM | TM | TM | TM | MCPsignal
					false,  // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
					false // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
				]
				    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
			})
			it('Filter protein sequences with 1 or more domains between 2 TM regions in the beginning of the sequence and with MCPsignal at the end', () => {
                const regArchPattern: IRegArch = {
                    patterns: [
                        {
                            npos: [],
                            pos: [
                                {
                                    name: '^',
                                    resource: 'ra'
                                },
                                {
                                    count: '{1}',
                                    name: 'TM',
                                    resource: 'das',
                                },
                                {
                                    count: '{1,}',
                                    name: '.*',
                                    resource: 'pfam28',
                                },
                                {
                                    count: '{1}',
                                    name: 'TM',
                                    resource: 'das',
                                },
                                {
                                    count: '{1,}',
                                    name: '.*',
                                    resource: 'pfam28',
                                },
                                {
                                    count: '{1}',
                                    name: 'MCPsignal',
                                    resource: 'pfam28',
                                },
                                {
                                    name: '$',
                                    resource: 'ra',
                                }
                            ]
                        }
                    ]
                }
				const expected = [
					false, // CheW | CheW
					false, // CheW | Response_reg
					false, // CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
					false, // CheW | CheW | CheW
					false, // Response_reg | NMT1_2 | CheW
					false, // CheW | CheR
					false, // Response_reg | CheW
					true, // TM | Cache_1 | TM | HAMP | MCPsignal
					false, // HAMP | MCPsignal
					false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
					false, // **
					false, // **
					false,  // TM | TM | MCPsignal
					false,  // TM | TM | MCPsignal | Rhodanese
					false, // TM | TM | TM | TM | MCPsignal
					false, // TM | TM | TM | TM | TM | TM | MCPsignal
					true,  // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
					true, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
					true, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
					true, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
					true // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
				]
				    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
			})
			it('Filter protein sequences starting with no Cache_1 between two TM and ending a MCPsignal', () => {
				const regArchPattern: IRegArch = {
                    patterns: [
                        {
                            npos: [],
                            pos: [
                                {
                                    name: '^',
                                    resource: 'ra'
                                },
                                {
                                    count: '{1}',
                                    name: 'TM',
                                    resource: 'das',
                                },
                                [
                                    {
                                        count: '{1,}',
                                        name: '.*',
                                        resource: 'pfam28',
                                    },
                                    {
                                        count: '{0}',
                                        name: 'Cache_1',
                                        resource: 'pfam28',
                                    },
                                ],
                                {
                                    count: '{1}',
                                    name: 'TM',
                                    resource: 'das',
                                },
                                {
                                    count: '{1,}',
                                    name: '.*',
                                    resource: 'pfam28',
                                },
                                {
                                    count: '{1}',
                                    name: 'MCPsignal',
                                    resource: 'pfam28',
                                },
                                {
                                    name: '$',
                                    resource: 'ra',
                                }
                            ]
                        }
                    ]
                }
				const expected = [
					false, // CheW | CheW
					false, // CheW | Response_reg
					false, // CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
					false, // CheW | CheW | CheW
					false, // Response_reg | NMT1_2 | CheW
					false, // CheW | CheR
					false, // Response_reg | CheW
					false, // TM | Cache_1 | TM | HAMP | MCPsignal
					false, // HAMP | MCPsignal
					false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
					false, // **
					false, // **
					false,  // TM | TM | MCPsignal
					false,  // TM | TM | MCPsignal | Rhodanese
					false, // TM | TM | TM | TM | MCPsignal
					false, // TM | TM | TM | TM | TM | TM | MCPsignal
					true,  // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
					true // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
				]
				    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
			})
			it('Filter protein sequences starting with TM followed by any two domains from pfam28 but no Cache_1 followed by another TM and ending a MCPsignal', () => {
                const regArchPattern: IRegArch = {
                    patterns: [
                        {
                            npos: [],
                            pos: [
                                {
                                    name: '^',
                                    resource: 'ra'
                                },
                                {
                                    count: '{1}',
                                    name: 'TM',
                                    resource: 'das',
                                },
                                [
                                    {
                                        count: '{1}',
                                        name: '.*',
                                        resource: 'pfam28',
                                    },
                                    {
                                        count: '{0}',
                                        name: 'Cache_1',
                                        resource: 'pfam28',
                                    },
                                ],
                                [
                                    {
                                        count: '{1}',
                                        name: '.*',
                                        resource: 'pfam28',
                                    },
                                    {
                                        count: '{0}',
                                        name: 'Cache_1',
                                        resource: 'pfam28',
                                    },
                                ],
                                {
                                    count: '{1}',
                                    name: 'TM',
                                    resource: 'das',
                                },
                                {
                                    count: '{1,}',
                                    name: '.*',
                                    resource: 'pfam28',
                                },
                                {
                                    count: '{1}',
                                    name: 'MCPsignal',
                                    resource: 'pfam28',
                                },
                                {
                                    name: '$',
                                    resource: 'ra',
                                }
                            ]
                        }
                    ]
                }
				const expected = [
					false, // CheW | CheW
					false, // CheW | Response_reg
					false, // CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
					false, // CheW | CheW | CheW
					false, // Response_reg | NMT1_2 | CheW
					false, // CheW | CheR
					false, // Response_reg | CheW
					false, // TM | Cache_1 | TM | HAMP | MCPsignal
					false, // HAMP | MCPsignal
					false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
					false, // **
					false, // **
					false,  // TM | TM | MCPsignal
					false,  // TM | TM | MCPsignal | Rhodanese
					false, // TM | TM | TM | TM | MCPsignal
					false, // TM | TM | TM | TM | TM | TM | MCPsignal
					true,  // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
					false // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
				]
				    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
			})
			it('Filter protein sequences starting with TM followed by any two domains from pfam28 but Cache_1 not in the first of the two followed by another TM and ending a MCPsignal', () => {
                const regArchPattern: IRegArch = {
                    patterns: [
                        {
                            npos: [],
                            pos: [
                                {
                                    name: '^',
                                    resource: 'ra'
                                },
                                {
                                    count: '{1}',
                                    name: 'TM',
                                    resource: 'das',
                                },
                                [
                                    {
                                        count: '{1}',
                                        name: '.*',
                                        resource: 'pfam28',
                                    },
                                    {
                                        count: '{0}',
                                        name: 'Cache_1',
                                        resource: 'pfam28',
                                    },
                                ],
                                {
                                    count: '{1}',
                                    name: '.*',
                                    resource: 'pfam28',
                                },
                                {
                                    count: '{1}',
                                    name: 'TM',
                                    resource: 'das',
                                },
                                {
                                    count: '{1,}',
                                    name: '.*',
                                    resource: 'pfam28',
                                },
                                {
                                    count: '{1}',
                                    name: 'MCPsignal',
                                    resource: 'pfam28',
                                },
                                {
                                    name: '$',
                                    resource: 'ra',
                                }
                            ]
                        }
                    ]
                }
				const expected = [
					false, // CheW | CheW
					false, // CheW | Response_reg
					false, // CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
					false, // CheW | CheW | CheW
					false, // Response_reg | NMT1_2 | CheW
					false, // CheW | CheR
					false, // Response_reg | CheW
					false, // TM | Cache_1 | TM | HAMP | MCPsignal
					false, // HAMP | MCPsignal
					false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
					false, // **
					false, // **
					false,  // TM | TM | MCPsignal
					false,  // TM | TM | MCPsignal | Rhodanese
					false, // TM | TM | TM | TM | MCPsignal
					false, // TM | TM | TM | TM | TM | TM | MCPsignal
					true,  // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
					true, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
					false // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
				]
				    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
			})
			it('Filter sequences that start with TM and end with MCPsignal.', () => {
                const regArchPattern: IRegArch = {
                    patterns: [
                        {
                            npos: [],
                            pos: [
                                {
                                    name: '^',
                                    resource: 'ra'
                                },
                                {
                                    count: '{1}',
                                    name: 'TM',
                                    resource: 'das',
                                },
                                {
                                    count: '{1,}',
                                    name: '.*',
                                    resource: '.*',
                                },
                                {
                                    count: '{1}',
                                    name: 'MCPsignal',
                                    resource: 'pfam28',
                                },
                                {
                                    name: '$',
                                    resource: 'ra',
                                }
                            ]
                        }
                    ]
                }
				const expected = [
					false, // CheW | CheW
					false, // CheW | Response_reg
					false, // CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
					false, // CheW | CheW | CheW
					false, // Response_reg | NMT1_2 | CheW
					false, // CheW | CheR
					false, // Response_reg | CheW
					true, // TM | Cache_1 | TM | HAMP | MCPsignal
					false, // HAMP | MCPsignal
					false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
					false, // **
					false, // **
					true,  // TM | TM | MCPsignal
					false,  // TM | TM | MCPsignal | Rhodanese
					true, // TM | TM | TM | TM | MCPsignal
					true, // TM | TM | TM | TM | TM | TM | MCPsignal
					true,  // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
					true, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
					true, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
					true, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
					true // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
				]
				    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
			})
			it('Filter protein sequences that starts with at least 3 domains of the PAS family using wildcards "PAS.*"', () => {
                const regArchPattern: IRegArch = {
                    patterns: [
                        {
                            npos: [],
                            pos: [
                                {
                                    name: '^',
                                    resource: 'ra'
                                },
                                {
                                    count: '{3}',
                                    name: 'PAS.*',
                                    resource: 'pfam28',
                                },
                            ]
                        }
                    ]
                }
				const expected = [
					false, // CheW | CheW
					false, // CheW | Response_reg
					false, // CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
					false, // CheW | CheW | CheW
					false, // Response_reg | NMT1_2 | CheW
					false, // CheW | CheR
					false, // Response_reg | CheW
					false, // TM | Cache_1 | TM | HAMP | MCPsignal
					false, // HAMP | MCPsignal
					true, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
					true, // **
					true, // **
					false,  // TM | TM | MCPsignal
					false,  // TM | TM | MCPsignal | Rhodanese
					false, // TM | TM | TM | TM | MCPsignal
					false, // TM | TM | TM | TM | TM | TM | MCPsignal
					false,  // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
					false // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
				]
				    const ra = new RegArch(regArchPattern)
                    const results = ra.exec(sampleData)
                    results.forEach((result, i) => {
                        expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                    })
                    expect(results).eql(expected)
			})
        })
    })
    describe('Complex queries', () => {
		describe('Same position specified in two rules should mean OR', () => {
			it('Filter sequences with starts with TM-Cache_1 or TM-Cache_2 and end in MCPsignal', () => {
                const regArchPattern: IRegArch = {
                    patterns: [
                        {
                            npos: [],
                            pos: [
                                {
                                    name: '^',
                                    resource: 'ra'
                                },
                                {
                                    count: '{1}',
                                    name: 'TM',
                                    resource: 'das',
                                },
                                {
                                    count: '{1}',
                                    name: 'Cache_2',
                                    resource: 'pfam28',
                                },
                                {
                                    count: '{1,}',
                                    name: '.*',
                                    resource: '.*',
                                },
                                {
                                    count: '{1}',
                                    name: 'MCPsignal',
                                    resource: 'pfam28',
                                },
                                {
                                    name: '$',
                                    resource: 'ra',
                                }
                            ]
                        },
                        {
                            npos: [],
                            pos: [
                                {
                                    name: '^',
                                    resource: 'ra'
                                },
                                {
                                    count: '{1}',
                                    name: 'TM',
                                    resource: 'das',
                                },
                                {
                                    count: '{1}',
                                    name: 'Cache_1',
                                    resource: 'pfam28',
                                },
                                {
                                    count: '{1,}',
                                    name: '.*',
                                    resource: '.*',
                                },
                                {
                                    count: '{1}',
                                    name: 'MCPsignal',
                                    resource: 'pfam28',
                                },
                                {
                                    name: '$',
                                    resource: 'ra',
                                }
                            ]
                        }
                    ]
                }
				const expected = [
					false, // CheW | CheW
					false, // CheW | Response_reg
					false, // CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
					false, // CheW | CheW | CheW
					false, // Response_reg | NMT1_2 | CheW
					false, // CheW | CheR
					false, // Response_reg | CheW
					true, // TM | Cache_1 | TM | HAMP | MCPsignal
					false, // HAMP | MCPsignal
					false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
					false, // **
					false, // **
					false, // TM | TM | MCPsignal
					false, // TM | MCPsignal | Rhodanese
					false, // TM | TM | TM | TM | MCPsignal
					false, // TM | TM | TM | TM | TM | TM | MCPsignal
					true,  // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
					true, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
					true, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
					true, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
					true // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
				]
                const ra = new RegArch(regArchPattern)
                const results = ra.exec(sampleData)
                results.forEach((result, i) => {
                    expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                })
                expect(results).eql(expected)
			})
		})
		describe('Combining Npos and pos rules', () => {
			it('Filter protein sequences that starts with TM-Cache1 and ends with MCPsignal but does not have Cache_2 anywhere', () => {
                const regArchPattern: IRegArch = {
                    patterns: [
                        {
                            npos: [
                                {
                                    count: '{0}',
                                    name: 'Cache_2',
                                    resource: 'pfam28',

                                }
                            ],
                            pos: [
                                {
                                    name: '^',
                                    resource: 'ra'
                                },
                                {
                                    count: '{1}',
                                    name: 'TM',
                                    resource: 'das',
                                },
                                {
                                    count: '{1}',
                                    name: 'Cache_1',
                                    resource: 'pfam28',
                                },
                                {
                                    count: '{1,}',
                                    name: '.*',
                                    resource: '.*',
                                },
                                {
                                    count: '{1}',
                                    name: 'MCPsignal',
                                    resource: 'pfam28',
                                },
                                {
                                    name: '$',
                                    resource: 'ra',
                                }
                            ]
                        }
                    ]
                }
				const expected = [
					false, // CheW | CheW
					false, // CheW | Response_reg
					false, // CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | CheW
					false, // Hpt | P2 | H-kinase_dim | HATPase_c | CheW | Response_reg
					false, // CheW | CheW | CheW
					false, // Response_reg | NMT1_2 | CheW
					false, // CheW | CheR
					false, // Response_reg | CheW
					true, // TM | Cache_1 | TM | HAMP | MCPsignal
					false, // HAMP | MCPsignal
					false, // PAS_9 | PAS | PAS_4 | PAS_3 | TM | TM | MCPsignal
					false, // **
					false, // **
					false, // TM | TM | MCPsignal
					false, // TM | MCPsignal | Rhodanese
					false, // TM | TM | TM | TM | MCPsignal
					false, // TM | TM | TM | TM | TM | TM | MCPsignal
					false,  // TM | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
					false, // TM | Cache_2 | Cache_1 | TM | HAMP | MCPsignal
					false, // TM | Cache_1 | Cache_2 | TM | HAMP | MCPsignal
					true, // TM | Cache_1 | Cache_1 | TM | HAMP | MCPsignal
					false // TM | Cache_2 | Cache_2 | Cache_2 | TM | HAMP | MCPsignal
				]
                const ra = new RegArch(regArchPattern)
                const results = ra.exec(sampleData)
                results.forEach((result, i) => {
                    expect(result, `${i} -> ${expected[i]} - ${archs[i]}`).eql(expected[i])
                })
                expect(results).eql(expected)
			})
		})
	})
})
