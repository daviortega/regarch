import { expect } from "chai";
import { validateArch } from "../src/RegArchUtils";

describe('RegArchUtils', () => {
    describe('validateArch', () => {
        it('should be ok with valid Archs', () => {
            const validArch = [ 
                "2|CheW@pfam28|139",
                "2|SM00260@smart|137",
                "166|SM00260@smart|311",
                "171|CheW@pfam28|314" 
            ]
            const parsedArch = validateArch(validArch)
            // tslint:disable-next-line: no-unused-expression
            expect(parsedArch).eql(validArch)
        })
        it('should throw if build with invalid Archs', () => {
            const invalidArch = [ 
                "2|CheW@pfa@m28|139",
            ]
            expect(() => {
                const parsedArch = validateArch(invalidArch)
            }).to.throw()
        })
    })
})