import { expect } from "chai"
import { RegArchParser } from "../src/RegArchParser"


describe('RegArchParser', () => {
    describe('check behaviour of build', () => {
        it('with empty pos', () => {
            const pattern = {
                npos: [
                    {
                        name: 'CheW',
                        resource: 'pfam28'
                    }
                ],
                pos: [],
                resources: new Set(['pfam28'])
            }
            const expected = {
                npos: [['CheW@pfam28', [1, 1]]],
                pos: [],
                resources: new Set(['pfam28'])
            }
            const interpreter = new RegArchParser()
            const parsedPattern = interpreter.build(pattern)
            expect(parsedPattern).eql(expected)
        })
        it('with empty pos but with wild cards all over', () => {
            const pattern = {
                npos: [
                    {
                        name: '.*',
                        resource: '.*'
                    }
                ],
                pos: [],
            }
            const expected = {
                npos: [['.*@.*', [1, 1]]],
                pos: [],
                resources: new Set(['.*'])
            }
            const interpreter = new RegArchParser()
            const parsedPattern = interpreter.build(pattern)
            expect(parsedPattern).eql(expected)
        })
        it('with missing npos', () => {
            const pattern = {
                npos: [],
                pos: [
                    {
                        name: 'CheW',
                        resource: 'pfam28'
                    },
                    {
                        name: 'Response_reg',
                        resource: 'pfam28'
                    }
                ],
                resources: new Set(['pfam28'])
            }
            const expected = {
                npos: [],
                pos: [
                    ['CheW@pfam28', [1, 1]],
                    ['Response_reg@pfam28', [1, 1]]
                ],
                resources: new Set(['pfam28'])
            }
            const interpreter = new RegArchParser()
            const parsedPattern = interpreter.build(pattern)
            expect(parsedPattern).eql(expected)
        })
        it('complex pattern', () => {
            const pattern = {
                npos: [
                    {
                        name: 'TM',
                        resource: 'das',
                    }
                ],
                pos: [
                    {
                        name: 'CheW',
                        resource: 'pfam28',
                    },
                    [
                        {
                            count: '{0}',
                            name: 'Response_reg',
                            resource: 'pfam28',
                        },
                        {
                            count: '{1}',
                            name: '.*',
                            resource: 'pfam28',
                        }
                    ]
                ],
                resources: new Set(['pfam28'])
            }
            const expected = {
                npos: [
                    ['TM@das', [1, 1]]
                ],
                pos: [
                    ['CheW@pfam28', [1, 1]],
                    [
                        ['Response_reg@pfam28', [0, 0]],
                        ['.*@pfam28', [1, 1]]
                    ]
                ],
                resources: new Set(['pfam28', 'das'])
            }
            const interpreter = new RegArchParser()
            const parsedPattern = interpreter.build(pattern)
            expect(parsedPattern).eql(expected)
        })
    })    
})
