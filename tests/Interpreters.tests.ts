import { expect } from 'chai'
import { parseArch, parseRegArch } from "../src/Interpreters";

import dataset from '../testdata/annotation-sample-input.json'
import arch from '../testdata/arch.parsed.json'

describe('Interpreter', () => {
    describe('parseArch', () => {
        it('should work', () => {
            const allParsed = []
            for (const data of dataset) {
                allParsed.push(parseArch(data))
            }
            expect(arch.parsed).eql(allParsed)
        })
    })
})
